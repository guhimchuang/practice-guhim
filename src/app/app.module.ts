import { AutoCompleteModule } from 'primeng/autocomplete';
import { DataService } from './data.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GuhimAngularComponent } from './guhim-angular/guhim-angular.component';
import { MainComponent } from './main/main.component';
import { MainChildComponent} from './main/main-child/main-child.component';
import { EmailValidator, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CardModule} from 'primeng/card';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './pages/login/login.component';
import { MainFooterComponent } from './main/main-footer/main-footer.component';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { PersonsModule } from './pages/login/persons/persons.module';
import { PersonsComponent } from './persons/persons.component';
import {TableModule} from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';






@NgModule({
  declarations: [
    AppComponent,
    GuhimAngularComponent,
    MainComponent,
   MainChildComponent,
   LoginComponent,
   MainFooterComponent,
   PersonsComponent,



  ],

  imports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    CardModule,
    InputTextModule,
    ButtonModule,
    AutoCompleteModule,
    BrowserAnimationsModule,
    TableModule,
    DropdownModule,


    // PersonsModule





  ],

  providers: [DataService],
  bootstrap: [AppComponent]
})

export class AppModule { }
