import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DataService } from '../data.service';
import { ArticleModel, CityModel, FilterModel } from '../interfaces/article';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.scss'],
})
export class PersonsComponent implements OnInit {
  form: FormGroup;
  cityArr: ArticleModel[];
  cityName: ArticleModel;

  locationArr: CityModel[];
  locationName: CityModel;
  locationArrNew: CityModel[];

  filterArr: FilterModel[];
  filterName: FilterModel;

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.getAllData();
    this.getCityData();
    // this.getFilterData();
    this.today = new Date();
    console.log(this.cityArr);
    console.log(this.locationArr);

    setInterval(() => {
      this.today = new Date();
    }, 1000);
  }

  today: Date;

  getFilterData(){
    this.dataService.getFilterData().subscribe((result) => {
      this.filterArr = result;
    });
  }

  getAllData() {
    this.dataService.getAllData().subscribe((result) => {
      this.cityArr = result;
    });
  }

  getCityData() {
    this.dataService.getCityData().subscribe((result) => {
      this.locationArr = result;
      this.locationArrNew = result;
      console.log(this.locationArr);
    });
  }
  search(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    let query = event.query; //搜尋的東西
    let taiwanData = [...this.locationArr]; //載進去23筆
    console.log(taiwanData);

    for (let i = 0; i < taiwanData.length; i++) {
      let taiwanCity = taiwanData[i]; //
      if (taiwanCity.location.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(taiwanCity);
        console.log(taiwanCity.location);
        this.locationArrNew.push(this.locationName);
        console.log(this.locationArr);
      }
      this.locationArrNew = filtered;

      console.log(this.locationArrNew);
    }
  }

  showResult() {
    this.form.patchValue(this.cityName);
  }

  showUpdateData(
    id: number,
    name: string,
    gender: number,
    cityName: string,
    date: number
  ) {
    this.form.patchValue({
      id: id,
      name: name,
      gender: gender,
      cityName: cityName,
      date: date,
    });
  }
}
