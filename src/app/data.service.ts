import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ArticleModel, CityModel, FilterModel } from './interfaces/article';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  options: any;
  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) {
    this.options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      responseType: 'json',
    };
  }

  run() {
    console.log('service.run');
  }

  /** 取得所有資料 */
  getAllData(): Observable<ArticleModel[]> {
    return this.http.get<ArticleModel[]>(`${this.apiUrl}/posts`);
  }

  getCityData(): Observable<CityModel[]> {
    return this.http.get<CityModel[]>(`${this.apiUrl}/locations`);
  }

  /** 取得單一筆資料 */
  getData() {
    return this.http.get<ArticleModel>(`${this.apiUrl}/posts/1`);
  }
  /** 新增單一筆資料 */
  insertData(request: ArticleModel) {
    return this.http.post(`${this.apiUrl}/posts`, request, this.options);
  }
  /** 更新某一筆資料 */
  updateData(request: ArticleModel, id: number) {
    return this.http.put(`${this.apiUrl}/posts/${id}`, request, this.options);
  }

  /** 刪除某一筆資料 */
  deleteData(id: number) {
    return this.http.delete(`${this.apiUrl}/posts/${id}`, this.options);
  }

  /**搜尋一筆資料 */
  filterData(filterData: FilterModel) {
    let params = new HttpParams();
    if("name" in filterData)
    params = params.append('name', filterData.name);
    params = params.append('gender', filterData.gender.toString());
    params = params.append('location', filterData.location);
    return this.http.get<ArticleModel>(`${this.apiUrl}/posts`, {
      ...this.options,
      params,
  });
}


}
