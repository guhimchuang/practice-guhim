export interface ArticleModel {
  formName: string,
  artName: string,
  ticketQuant: number,
  ticketPrice: number,
  id?: number,
  cityName?: string,
  name?: string,
  gender?: number,
  date?: string,
  location?: string,

}

export interface CityModel {
  location?: string,
}

export interface FilterModel {
  name?: string,
  gender?: number,
  location?: string,
}
