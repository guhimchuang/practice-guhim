import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PersonModel } from 'src/app/guhim-angular/guhim';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  mainUrl = environment.apiUrl;


  constructor(private http: HttpClient) { }

  getMainData(): Observable<PersonModel[]> {
    return this.http.get<PersonModel[]>(`${this.mainUrl}/posts`);
  }
//   getMainDate(): PersonModel[] {
//   return [{name: 'Guhim', height: 169, birthday: new Date('2020-01-01') },
//   {name: 'Anna', height: 160, birthday: new Date('2020-02-01') },
//   {name: 'Sean', height: 178, birthday: new Date('2020-03-01') },
//   {name: 'Archer', height: 180, birthday: new Date('2020-04-01') }];
// }
}



