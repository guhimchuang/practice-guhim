import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainChildComponent } from './main/main-child/main-child.component';
import { MainFooterComponent } from './main/main-footer/main-footer.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { PersonsComponent } from './persons/persons.component';


const routes: Routes = [
  {
  path: '',
  component: MainComponent,
  children: [
    {
      path: 'main-child',
      component: MainChildComponent,
  }]
}, {
  path: 'login',
  component: LoginComponent,
},

{
  path: 'main-footer',
  component: MainFooterComponent,
},
{
  path: 'persons',
  component: PersonsComponent,
},
// 預設放在最後一層
{
  path:'**',
  redirectTo: 'login',
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
