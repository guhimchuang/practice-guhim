import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  invalidForm: FormGroup;
  form: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  goHome(): void {
    this.router.navigate(['/']);
  }



  /**檢查第二個表單狀態 */
  checkForm() {
    console.log('第二個form', this.invalidForm);
    if (this.invalidForm.invalid) {
      alert('驗證未通過');
      this.router.navigate(['/login']);
      return;
    }
    alert('驗證通過');
    this.router.navigate(['/persons']);
  }

  createForm() {
    this.invalidForm = this.fb.group({
      myUserName: [null, Validators.required],
      myEmail: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
        ]),
      ],
    });
  }
}
