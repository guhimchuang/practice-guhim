export interface PersonModel {
  name : string;
  height: number;
  weight?: number;
  birthday?: Date
}
