import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuhimAngularComponent } from './guhim-angular.component';

describe('GuhimAngularComponent', () => {
  let component: GuhimAngularComponent;
  let fixture: ComponentFixture<GuhimAngularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuhimAngularComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuhimAngularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
