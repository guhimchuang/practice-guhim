import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,

} from '@angular/forms';
import { PersonModel } from '../guhim-angular/guhim';
import { MainService } from '../service/main/main.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  invalidForm: FormGroup;
  OnInit(): void {
    throw new Error('Method not implemented.');
  }
  person: PersonModel = {
    name: 'guhim',
    height: 169,
    weight: 60,
  };

  currentClasses = {
    'ngclass-example': true,
    'ngclass-example2': true,
  };

  name = '卡納赫拉';
  hero: PersonModel[];
  styleExample: string;
  today: Date;
  formName = new FormControl('');
  form: FormGroup;
  get nameArr() {
    return this.form.get('nameArr') as FormArray;
  }
  constructor(private mainService: MainService, private fb: FormBuilder) {
    console.log('main:', this.mainService.mainUrl);
  }

  ngOnInit(): void {
    this.createForm();
    this.getAllData();

    this.today = new Date();
    setInterval(() => {
      this.today = new Date();
    }, 1000);
  }

  /**建立Form表單 */
  /** 若不給值: formName: [{value:'123', disabled: true}, Validators.required] */
  createForm() {
    this.form = this.fb.group({
      formName: [{ value: 'Guhim', disabled: true }, Validators.required],
      firstName: ['', Validators.required],
      secondName: ['', Validators.required],
      nameArr: this.fb.array([]),
    });

    this.form.get('formName').valueChanges.subscribe((result) => {
      console.log('valuechange', result);
    });
    this.createFormArr();
    this.invalidForm = this.fb.group({
      myUserName: [null, Validators.required],
      myNum: [
        null,
        Validators.compose([Validators.required, Validators.max(5)]),
      ],
      myPsw: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(`(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,30}`),
        ]),
      ],
    });

  }
  /** 建立FormArray */
  //push() Validators.required 必填欄位
  createFormArr() {
    this.nameArr.push(
      this.fb.group({
        childName: ['老大名字', Validators.required],
        secondChildName: ['老二名字', Validators.required],
      })
    );
  }
 
  /**檢查第二個表單狀態 */
  checkForm(){
    console.log('第二個form',this.invalidForm);
    if (this.invalidForm.invalid) {
      alert('驗證未通過');
      return;
    }
    alert('驗證通過');

  }
  /**更新表單的元件值 */
  // formGroup層次
  updateControlValue() {
    this.form.patchValue({
      firstName: 'patchValue可更新多個',
      secondName: '更新表單元件值成功',
    });
    /** 設置驗證器 */
    this.form.get('firstName').setValidators(Validators.minLength(5));
    /** 清除驗證器 */
    this.form.get('firstName').clearValidators();
    /** 變更驗證器時更新 */
    this.form.get('firstName').updateValueAndValidity();

    this.form.setControl('formName', new FormControl('替換控制元件'));

    // formControl層次
    // this.form.get('firstName').setValue('更新表單元件值成功');
    // this.form.get('secondName').patchValue('更新表單的控制值成功');
  }

  /**重設表單 呼叫reset方法 */
  resetForm() {
    this.form.reset();
  }

  /**取得英雄列表資料 */
  getAllData() {
    this.mainService.getMainData().subscribe((result) => {
      this.hero = result;
    });
  }

  myEnter2(input) {
    console.log(1);
    if (input.value === '卡納赫拉') {
      this.currentClasses['ngclass-example'] = false;
      this.currentClasses['ngclass-example2'] = true;
      // this.styleExample = 'pink';
    } else {
      console.log(2);
      this.currentClasses['ngclass-example'] = true;
      this.currentClasses['ngclass-example2'] = false;
      // this.styleExample = 'red';
    }

    this.styleExample = input.value === '卡納赫拉' ? 'pink' : 'red';
  }

  /** 改變當前名稱 */
  updateName(name: string) {
    this.name = name;
  }

  /**formControl 取值
   * getRawValue可以取disabled的值
   */
  getFormValue() {
    console.log('form:', this.form);
    console.log('value:', this.form.value);
    console.log('getRawValue:', this.form.getRawValue());
    console.log('valid:', this.form.valid);
  }

  // switch(type) {
  //   case 1:
  //     console.log(123);
  //     break;
  //   default:
  //     console.log('default');
  //     break;
  // }
}
