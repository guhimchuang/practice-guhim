import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { DataService } from 'src/app/data.service';
import { ArticleModel } from 'src/app/interfaces/article';
import { MainService } from 'src/app/service/main/main.service';
import { AutoCompleteModule } from 'primeng/autocomplete';

@Component({
  selector: 'app-main-footer',
  templateUrl: './main-footer.component.html',
  styleUrls: ['./main-footer.component.scss'],
})
export class MainFooterComponent implements OnInit {
  form: FormGroup;
  artArr: ArticleModel[];
  nextId: any;
  formName: string;
  concertArr: ArticleModel[];
  concertName: ArticleModel;
  id: number;
  // artName: string;
  // ticketQuant: number;
  // ticketPrice: number;

  /** 引入API */
  constructor(
    private mainService: MainService,
    private fb: FormBuilder,
    private dataService: DataService
  ) {
    console.log('main:', this.mainService.mainUrl);
  }

  ngOnInit(): void {
    this.createFormTicket1();
    this.getAllData();

    //filter
  }

  //filter
  search(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    let query = event.query;
    let artArr = [...this.artArr];
    console.log(artArr);
    for (let i = 0; i < artArr.length; i++) {
      let artName = artArr[i];
      if (artName.artName.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(artName);
      }
    }

    this.concertArr = filtered;
  }

  showResult() {
    this.form.patchValue(
      this.concertName
    );
    // this.form.patchValue({
    //   id: this.concertName.id,
    //   formName: this.concertName.formName,
    //   artName: this.concertName.artName,
    //   ticketQuant: this.concertName.ticketQuant,
    //   ticketPrice: this.concertName.ticketPrice,
    // });
  }

  getAllData() {
    this.dataService.getAllData().subscribe((result) => {
      this.artArr = result;
      this.concertArr = result;
    });
  }

  /** 新增一筆資料 */
  insertData() {
    this.dataService
      .insertData({
        id: this.form.value.id,
        formName: this.form.value.formName,
        artName: this.form.value.artName,
        ticketQuant: this.form.value.ticketQuant,
        ticketPrice: this.form.value.ticketPrice,
      })

      .subscribe((result) => {
        console.log(result);
        alert('新增成功');
      });
  }

  createFormTicket1() {
    this.form = this.fb.group({
      id: [{ value: '1', disabled: true }, Validators.required],
      formName: ['Guhim', Validators.required],
      artName: ['2021大港開唱音樂祭', Validators.required],
      ticketQuant: ['1', Validators.required],
      ticketPrice: ['1500', Validators.required],
    });
    this.form.get('formName').valueChanges.subscribe((result) => {
      console.log('valuechange', result);
    });
    aliases: this.fb.array([this.fb.control('')]);
  }

  // 顯示資料庫資料
  showUpdateData(
    id: number,
    formName: string,
    artName: string,
    ticketQuant: number,
    ticketPrice: number,
  ) {
    this.form.patchValue({
      id: id,
      formName: formName,
      artName: artName,
      ticketQuant: ticketQuant,
      ticketPrice: ticketPrice,
    });
  }
  // 更新資料
  submitData() {
    this.id = this.form.getRawValue().id;
    this.dataService
      .updateData(
        {
          formName: this.form.value.formName,
          artName: this.form.value.artName,
          ticketQuant: this.form.value.ticketQuant,
          ticketPrice: this.form.value.ticketPrice,
          
        },

        this.id
      )
      .subscribe((result) => {
        console.log(result);
        this.getAllData();
      });
  }

  updateControlValue() {
    this.form.patchValue({
      formName: '更新成功',
      secondName: '更新成功',
    });
    /** 設置驗證器 */
    this.form.get('formName').setValidators(Validators.minLength(5));
    /** 清除驗證器 */
    this.form.get('formName').clearValidators();
    /** 變更驗證器時更新 */
    this.form.get('formName').updateValueAndValidity();

    // this.form.setControl('formName', new FormControl('替換控制元件'));
  }

  /**重設表單 呼叫reset方法 */
  resetForm() {
    this.form.reset();
  }

  getFormValue() {
    console.log('form:', this.form);
    console.log('value:', this.form.value);
    console.log('getRawValue:', this.form.getRawValue());
    console.log('valid:', this.form.valid);
  }

  get aliases() {
    return this.form.get('aliases') as FormArray;
  }

  addAlias() {
    this.aliases.push(this.fb.control(''));
  }
  /**建立資料方式 */
  //   createData() {
  //     if (this.artArr === null) {
  //       return;
  //     }
  //     const req: ArticleModel = {
  //       id: this.artArr,
  //       title: "json-server",
  //       author: "typicode",
  //       content: "測試資料" + this.nextId,
  //       toggle: true
  //     };
  //     this.dataService.insertData(req).subscribe((result) => {
  //     this.getAllData();
  //   });
  // }

  /** 刪除資料的方式 */
  deleteCard(id) {
    this.dataService.deleteData(id).subscribe((result) => {
      console.log('deleteApi:', result);
      this.getAllData();
    });
  }
  // filter
}
// myConcert() {
//   document.getElementById("myDropdown").classList.toggle("show");
// }
// filterData(){
//   console.log(this.artName);
//   var input = document.getElementById("myInput");
//   var filter = input.nodeValue.toUpperCase();
//   var div = document.getElementById("myDropdown");
//   var a = div.getElementsByTagName("a");

// filterFunction() {
//   var input, filter, ul, li, a, i;
//   input = document.getElementById("myInput");
//   filter = input.value.toUpperCase();

//   div = document.getElementById("myDropdown");
//   a = div.getElementsByTagName("a");
//   for (i = 0; i < a.length; i++) {
//     txtValue = a[i].textContent || a[i].innerText;
//     if (txtValue.toUpperCase().indexOf(filter) > -1) {
//       a[i].style.display = "";
//     } else {
//       a[i].style.display = "none";
//     }
//   }
// }
