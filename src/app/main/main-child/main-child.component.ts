import { debugOutputAstAsTypeScript } from '@angular/compiler';
import { AfterContentInit, AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-main-child',
  templateUrl: './main-child.component.html',
  styleUrls: ['./main-child.component.scss']
})
export class MainChildComponent implements OnInit, OnDestroy, AfterViewInit, AfterContentInit {
  @Input() parentName: string;
  @Input() name: string;
  /**new 事件發送器 EventEmitter */
  @Output() updateName = new EventEmitter();

  constructor() { }




  ngOnInit(): void {

  }
  ngOnDestroy(): void {

  }
  ngAfterViewInit(): void {

  }
  ngAfterContentInit(): void {

  }

  /**通知爸爸改變身高   */
  callUpdateName() {
    console.log('觸發小孩的click事件');
    this.updateName.emit('P助');   //this代表同一個class
  }



}
